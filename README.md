## Meet the Rick and Morty Characters

This app displays the Rick & Morty character profiles by utilizing their WEB API. 


---

## Running the project

To run the project, simply clone the repo to your local machine and run index.html.  The app is built with simple HTML/CSS and vanilla Javascript and does not require any build tools or frameworks to run.
